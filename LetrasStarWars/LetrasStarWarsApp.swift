//
//  LetrasStarWarsApp.swift
//  LetrasStarWars
//
//  Created by Carlos Vera Baca on 19/05/21.
//

import SwiftUI

@main
struct LetrasStarWarsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

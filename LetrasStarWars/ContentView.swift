//
//  ContentView.swift
//  LetrasStarWars
//
//  Created by Carlos Vera Baca on 19/05/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hola Mundo esto es una prueba de un texto largo y proparemos el efecto de letras tipo Star Wars jkll iuoi huuhiu dafadf afafa dsg dffkjk jkj ;kjdsadef f uhiuhu iuhiuhiuh fdsgsg sdgssdlk dsvsvsdvsdvsv sdfsf sdfsfsfgsd de modificadores de texto en SwiftUI")
            .fontWeight(.regular)
            .font(.custom("Gill Sans", size: 25))
            .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
            .multilineTextAlignment(.center)
            .lineLimit(nil)
            .truncationMode(.middle)
            .padding(30)
            .lineSpacing(8)
            .rotation3DEffect(.degrees(50), axis: (x: 1 , y: 0, z:0) )
            .shadow(color: .gray, radius: 1, x: 0, y: 5 )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
